package com.copernic.lscardino.homersimpson_lorenzo;

import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Animation animacionRotarAgujas, animacionRotarContrarioLento,animacionRotarContrarioRapido, animacionDonut, animacionrotarDonut, animacionOjo;
    AnimationSet conjuntoDonut;
    AnimationDrawable animationTitulo;
    ImageView eng_rojo,eng_azul,eng_verde,ojo,donut;
    ImageButton imagenesTitulo;
    MediaPlayer audio;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagenesTitulo = findViewById(R.id.imagenTitulo);
        imagenesTitulo.setBackgroundResource(R.drawable.animacion_titulo);
        animationTitulo = (AnimationDrawable) imagenesTitulo.getBackground();
        animationTitulo.start();

        eng_rojo = findViewById(R.id.eng_rojo);
        eng_azul = findViewById(R.id.eng_azul);
        eng_verde = findViewById(R.id.eng_verde);
        ojo = findViewById(R.id.ojo);
        donut = findViewById(R.id.donut);

        animacionRotarAgujas = AnimationUtils.loadAnimation(MainActivity.this,R.anim.rotar_agujas_reloj);
        animacionRotarAgujas.setFillAfter(true);

        animacionRotarContrarioLento =AnimationUtils.loadAnimation(MainActivity.this,R.anim.rotar_sentido_contratio_lento);
        animacionRotarContrarioLento.setFillAfter(true);

        animacionRotarContrarioRapido =AnimationUtils.loadAnimation(MainActivity.this,R.anim.rotar_sentido_contrario_rapido);
        animacionRotarContrarioRapido.setFillAfter(true);

        animacionDonut =AnimationUtils.loadAnimation(MainActivity.this,R.anim.animacion_hacia_abajo);
        animacionDonut.setFillAfter(true);

        animacionrotarDonut = AnimationUtils.loadAnimation(MainActivity.this,R.anim.rotar_donut);
        animacionrotarDonut.setFillAfter(true);

        conjuntoDonut = new AnimationSet(false);
        conjuntoDonut.addAnimation(animacionrotarDonut);
        conjuntoDonut.addAnimation(animacionDonut);

        animacionOjo = AnimationUtils.loadAnimation(MainActivity.this,R.anim.rotar_ojo);
        animacionOjo.setFillAfter(true);


        invisibles();

        donut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(audio != null && audio.isPlaying()){
                    Toast.makeText(MainActivity.this, getString(R.string.parar), Toast.LENGTH_SHORT).show();
                    audio.stop();
                }else {
                    Toast.makeText(MainActivity.this, getString(R.string.audioOn), Toast.LENGTH_SHORT).show();

                    audio = MediaPlayer.create(MainActivity.this, R.raw.the_simpsons);
                    audio.start();
                }
            }
        });

        imagenesTitulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eng_rojo.getVisibility() == View.INVISIBLE){
                    visibles();
                }else{
                    invisibles();
                }
            }
        });
    }


    void invisibles(){
        acabarAnimaciones();
        eng_verde.setVisibility(View.INVISIBLE);
        eng_azul.setVisibility(View.INVISIBLE);
        eng_rojo.setVisibility(View.INVISIBLE);
        ojo.setVisibility(View.INVISIBLE);
        donut.setVisibility(View.INVISIBLE);
    }

    void visibles(){
        eng_verde.setVisibility(View.VISIBLE);
        eng_azul.setVisibility(View.VISIBLE);
        eng_rojo.setVisibility(View.VISIBLE);
        ojo.setVisibility(View.VISIBLE);
        donut.setVisibility(View.VISIBLE);
        empezarAnimaciones();
    }

    void empezarAnimaciones(){
        eng_rojo.startAnimation(animacionRotarContrarioLento);
        eng_verde.startAnimation(animacionRotarAgujas);
        eng_azul.startAnimation(animacionRotarContrarioRapido);
        donut.startAnimation(conjuntoDonut);
        ojo.startAnimation(animacionOjo);
    }

    //No es la forma más eficiente, lo sé.
    void acabarAnimaciones(){
        eng_rojo.clearAnimation();
        eng_azul.clearAnimation();
        eng_verde.clearAnimation();
        donut.clearAnimation();
        ojo.clearAnimation();

    }

}
